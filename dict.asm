%include "lib.inc"
global find_word

section .text



; Если подходящее вхождение найдено, 
; вернёт адрес начала вхождения в словарь (не значения),
; иначе вернёт 0.

; rdi - Указатель на нуль-терминированную строку.
; rsi - Указатель на начало словаря.

find_word:
	.loop:
		test rsi, rsi
		jz .err
		add rsi, 8
		call string_equals
		cmp rax, 1
		jz .success
		sub rsi, 8
		mov rsi, [rsi]
		jmp .loop
	.err:
		xor rax, rax
		ret
	.success:
		sub rsi, 8
		mov rax, rsi
		ret
