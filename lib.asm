global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy

%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0

%define STDOUT 1
%define STDIN 0
%define STDERR 2

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout


;rdi указатель на строку
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    syscall
    ret

print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    mov rdi, STDERR
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'

; Принимает код символа и выводит его в stdout


;rdi код символа
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 1
    xor r9, r9
    mov r9, 10
    dec rsp
    mov byte[rsp], 0
    .loop:
        inc r8
        xor rdx, rdx
        div r9
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        jnz .loop       
    .print:
        mov rdi, rsp
        push r8
        call print_string
        pop r8
        add rsp, r8
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе


;rdi указатель на первую строку
;rsi указатель на вторую строку
string_equals:
    xor rcx, rcx
    .loop:
        mov dl, [rdi + rcx]
        cmp dl, [rsi + rcx]
        jne .not_equal
        inc rcx
        test rdx, rdx
        jne .loop
        mov rax, 1
        ret
    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока

read_char:
    xor rax, rax 
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    mov rax, SYS_READ
    mov rdi, STDIN
    syscall
    cmp rax, 0
    jle .end
    xor rax, rax
    mov rax, [rsp]
    .end:
        inc rsp
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


;rdi начало буфера
;rsi размер
read_word:
    push r12
    mov r12, rdi
    dec rsi
    push r13
    mov r13, rsi
    push r14
    xor r14, r14
    .loop:
        cmp r14, r13
        je .buf_overflow
        call read_char
        test al, al
        jz .end
        cmp al, 0x20
        je .skip
        cmp al, 0x9
        je .skip
        cmp al, 0xA
        je .skip
        mov [r12 + r14], al
        inc r14
        jmp .loop
    .buf_overflow:
        xor rax, rax
        jmp .exit
    .skip:
        test r14, r14
        jz .loop
        jmp .end
    .end:
        mov byte[r12 + r14], 0
        mov rax, r12
        mov rdx, r14
    .exit:
        pop r14
        pop r13
        pop r12
        ret
 


;rdi начало буфера
;rsi размер
read_line:
	push 12
	push r13
    push r14
	mov	r12, rdi
	mov	r13, rsi
    xor r14, r14
	.loop:
        test r14, r13
		ja .buf_overflow
		call read_char
		test al, al
        jz .end
		cmp	al, 0x20
		je .skip
		cmp	al, 0x9
		je .skip
		cmp al, 0xA
		je .skip
		jmp .skip_test
	.read_word:
        cmp r14, r13
		ja .buf_overflow
		cmp	al, 0x9
		je .end
		cmp	al, 0xA
		je .end
		test al, al
		je .end
	.skip_test:
		mov	byte [r12 + r14], al
        inc r14
		call read_char
		jmp .read_word

    .skip:
        test r14, r14
        jz .loop
        jmp .end

	.end:
        ;inc r14
		mov	byte [r12 + r14], 0
		mov	rax, r12
		mov	rdx, r14
		jmp .exit
	.buf_overflow:
		xor rax, rax
	.exit:
        pop r14
        pop	r13
		pop r12
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось


;rdi указатель на строку
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
    .loop:
        mov dl, byte[rdi + rcx]
        sub dl, '0'
        test dl, dl
        js .end
        cmp dl, 10
        jns .end
        imul rax, 10
        add rax, rdx
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось


;rdi указатель на строку
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    jz .negative
    cmp byte[rdi], '+'
    jne parse_uint
    inc rdi
    call parse_uint
    jmp .end
    .negative:
        inc rdi
        call parse_uint
        neg rax
    .end:
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0


;rdi указатель на строку
;rsi указатель на буфер
;rdx длина буфера
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        je .buf_overflow
        mov cl, [rdi + rax]
        mov [rsi + rax], cl
        inc rax
        test cl, cl
        js .end
        jmp .loop
    .buf_overflow:
        xor rax, rax
    .end:
        ret
