%include "dict.inc"
%include "lib.inc"
%include "words.inc"

global _start

section .rodata
error_message: db "invalid key input", 10, 0
not_found_message: db "element not found", 10, 0

%define buffer_size 255
%define address_size 8

section .bss
buffer: resb buffer_size

section .text
_start:
	mov rdi, buffer
	mov rsi, buffer_size
	call read_line
	mov r12, rdx
	test rax, rax
	jz .error
	mov rdi, buffer
	mov rsi, head
	call find_word
	test rax, rax
	jz .not_found
	mov rdi, rax
	add rdi, address_size
	add rdi, r12
	inc rdi
	call print_string
	xor rdi, rdi
	jmp exit
	.error:
		mov rdi, error_message
		call print_error
		mov rdi, 1
		jmp exit
	.not_found:
		mov rdi, not_found_message
		call print_error
		mov rdi, 1
		jmp exit