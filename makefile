ASM=nasm
ASMFLAGS=-felf64 -g
LD=ld

main.o: main.asm dict.inc lib.inc words.inc
dict.o: dict.asm lib.inc
words.inc: colon.inc
	
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main: lib.o main.o dict.o
	$(LD) -o main main.o lib.o dict.o

test: main
	python3 test.py
	
.PHONY: clean test
clean:
	rm  main.o lib.o dict.o 