from subprocess import Popen, PIPE

inputs = ["key1", "key2", "key3", "a", "fourth word", "a" * 256, "a" * 255]
outputs = ["word1", "word2", "word3", "", "word4 explanation", "", ""]
errors = ["", "", "", "element not found", "", "invalid key input", "element not found"]
exit_codes = [0, 0, 0, 1, 0, 1, 1]

for i in range(len(inputs)):
    process = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    data = process.communicate(inputs[i].encode())
    num = str(i + 1)
    if data[0].decode().strip() == outputs[i] and data[1].decode().strip() == errors[i] and process.returncode == exit_codes[i]:
        print(f"test {str(i + 1)} done: \"{inputs[i]}\"")
    else:
        print(f"test {str(i + 1)} failed: \"{data[0].decode().strip()} \", stderr: \"{data[1].decode().strip()}\". \n\texpected result: \"{outputs[i]}\", stderr: \"{errors[i]}\", exitcode: \"{exit_codes[i]}\"")